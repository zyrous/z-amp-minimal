# z-amp-minimal

**A simple but stylish audio player for the web, built with z-amp-core**

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/4c6e05f1dd77404f9e1ce10ea6927928)](https://www.codacy.com/bb/zyrous/z-amp-minimal/dashboard?utm_source=mason_zyrous@bitbucket.org&amp;utm_medium=referral&amp;utm_content=zyrous/z-amp-minimal&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/4c6e05f1dd77404f9e1ce10ea6927928)](https://www.codacy.com/bb/zyrous/z-amp-minimal/dashboard?utm_source=mason_zyrous@bitbucket.org&utm_medium=referral&utm_content=zyrous/z-amp-minimal&utm_campaign=Badge_Coverage)
[![z-amp-minimal](https://img.shields.io/endpoint?url=https://dashboard.cypress.io/badge/detailed/w1j9cq/master&style=flat&logo=cypress)](https://dashboard.cypress.io/projects/w1j9cq/runs)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

![Minimal Theme Demo](https://bitbucket.org/zyrous/z-amp-minimal/raw/ec59f84fe7a3ba564b5eeb14d4b8588416d34a9c/docs/assets/minimal-overview.gif)

The Minimal theme includes the following Z-Amp components:
-   Audio Player
-   Playlist Manager
-   Equalizer
-   Audio HTML Visualizer
## Installation
At present, the Minimal theme is installable as an importable script in your site's markup. In the ```<head>``` section of the page, include the following code snippet:
```html
<script type="text/javascript" src="https://unpkg.com/z-amp-minimal/build/z-amp-minimal.bundle.min.js"></script>
```
## Quick Start
```html
<script type="text/javascript">
    window.onload = () => {
        ZAmp.amp()
        .then((amp) => {
            amp.playlist.clear();
            amp.playlist.addTrack({url: "<TRACK URL>", title: "<TRACK TITLE>", artist: "<TRACK ARTIST>"});
        });
    }
</script>
```
Make sure to replace the variables with your own values. Repeat the ```addTrack``` statement for any number of tracks that you wish to add to the playlist.

## Features
### Auto-compacting design
Minimal will ensure that it takes up the least amount of screen real-estate possible. When it's not being interacted with (doesn't have focus or mouse over), it will reduce its size to include only a play/pause button and the name of the current song (if it's playing).

### Hidden controls
When the player is selected it automatically reveals next, previous and play/pause buttons, as well as controls for shuffle and loop.

### Playlist
One of the Minimal theme's selectable panels is a playlist that contains all of the player's current tracks. Select one to change the track or scroll through the list to find the track you're looking for. The current track is marked with a timer and a small visual EQ.

### Equalizer
The second of the Minimal theme's selectable panels allows you to choose from three different presets of a seven-band EQ: Bass, Rock or Club. Manually adjusting one of the frequencies automatically selects Custom mode.

## Further Support
For more information about what you can do with Z-Amp, check out the website for further documentation.
