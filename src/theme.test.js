const chai = require("chai");
chai.use(require("chai-as-promised"));
const expect = chai.expect;
const proxyquire = require("proxyquire").noCallThru();
const sinon = require("sinon");

// NOTE: Because importing the theme will cause the Z-Amp core to attach
// objects to the window object immediately, we must ensure that there's
// something to attach to.
const JSDOM = require("jsdom");
const dom = new JSDOM.JSDOM();
global.window = dom.window;
global.document = dom.window.document;

// For testing, we'll have a proxy for our theme and stubs for each of
// its external files.
var themeProxy, layoutStub, stylesStub;

// The sandbox that will hold all of our mocks.
sinon.createSandbox();

describe("Minimal Theme", function() {
    this.timeout(10000);

    beforeEach(() => {
        // We need to mock out the external pug and style files immediately.
        // Browserify knows how to handle those but NodeJS doesn't.
        layoutStub = sinon.stub();
        stylesStub = sinon.stub(); 
        themeProxy = proxyquire("./theme", { "./layout.pug": layoutStub, "./styles.css": stylesStub });
    });

    it("Should be constructed with the correct listeners", () => {
        var theme = new themeProxy.MinimalTheme();
        expect(theme.eventListeners.get("frequencyChanged")).to.exist;
        expect(theme.eventListeners.get("trackIndexChanged")).to.exist;
        expect(theme.eventListeners.get("playlistLoaded")).to.exist;
        expect(theme.eventListeners.get("playlistUpdated")).to.exist;
    });
});