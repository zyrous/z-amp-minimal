const { Theme } = require("z-amp-core");
const layout = require("./layout.pug");
const styles = require("./styles.css");
const corejs = require("core-js");
const regenerator = require("regenerator-runtime/runtime");

/**
 * @namespace WebAmp.Components.Theme.Minimal
 */

/**
 * Provides the default theme for WebAmp.
 * @author Mason Yarrick <mason.yarrick@zyrous.com>
 * @memberof WebAmp.Components.Theme.Minimal
 * @augments Theme
 */
class MinimalTheme extends Theme {

    /**
     * Play/pause button.
     * @private
     * @type {HTMLElement}
     */
    playPauseButton;

    /**
     * Container for the entire player.
     * @private
     * @type {HTMLElement}
     */
    playerContainer;

    /**
     * The button to open the playlist.
     * @private
     * @type {HTMLElement}
     */
    playlistButton;

    /**
     * The container of the button to open the playlist.
     * @private
     * @type {HTMLElement}
     */
    playlistButtonContainer;

    /**
     * The button to open the equalizer.
     * @private
     * @type {HTMLElement}
     */
    eqButton;

    /**
     * The container of the button to open the equalizer.
     * @private
     * @type {HTMLElement}
     */
    eqButtonContainer;

    /**
     * The set of sliders that will be used to change band frequencies.
     * @private
     * @type {HTMLElement}
     */
    eqSliders;

    /**
     * The container that holds the equalizer.
     * @private
     * @type {HTMLElement}
     */
    eqContainer;

    /**
     * The container that holds the playlist.
     * @private
     * @type {HTMLElement}
     */
    playlistContainer;

    /**
     * The container for the playlist visualiser.
     * @private
     * @type {HTMLElement}
     */
    playlistVisualiserContainer;

    /**
     * The container for the playlist's playtime.
     * @private
     * @type {HTMLElement}
     */
    playlistPlaytimeContainer;

    /**
     * The panel that should be shown to the user.
     * @private
     * @type {String}
     */
    selectedPanel;

    /**
     * The height that the playlist panel should be set to.
     * @private
     * @type {Number}
     */
    playlistHeight;

    /**
     * Whether or not the player is currently in the expanded state.
     * @private
     * @type {Boolean}
     */
    expanded = false;

    /**
     * The height (in px) of a single playlist item.
     * @private
     * @type {Number}
     */
    _playlistItemHeight = 47;

    /**
     * The padding that should be added to the bottom of the playlist.
     * @private
     * @type {Number}
     */
    _playlistBottomPadding = 32;

    /**
     * The maximum deviation (in dB) from zero that should be allowed for frequencies.
     * @private
     * @type {Number}
     */
    _frequencyMaxDistortion = 12;

    /**
     * Construct a new instance of the default WebAmp theme.
     */
    constructor(){
        super();

        // Set the name of the theme.
        this.themeName = "Minimal";

        // We need to listen for when frequencies are changed so that we can trigger the updating
        // of the style for each range element.
        this.addEventListener("frequencyChanged", this.onFrequencyChanged);

        // Listen to the track changed and audio playing events so that we can reposition the 
        // visualiser and track info in the playlist.
        this.addEventListener("trackIndexChanged", this.onTrackChanged);

        this.addEventListener("playlistLoaded", (tracks) => this.recalculatePlaylistHeight(tracks.length));
        this.addEventListener("playlistUpdated", (tracks) => this.recalculatePlaylistHeight(tracks.length));
    }

    /**
     * Load the previous state for the theme.
     * @protected
     */
    async loadState() {
        // Figure out which panel was selected before (playlist or EQ).
        this.getValue("selectedPanel", "playlist")
        .then((result) => this.showPanel(result.value));
    }

    /**
     * Connect to the on-screen elements of the theme that we care about.
     * @protected
     */
    async initialiseElements() {
        super.initialiseElements();

        // Play/pause button.
        this.attachElement(this, "playPauseButton", "[audio-button-play-pause]", {
            eventName: "click",
            callback: this.expand   // Always expand when play or pause is clicked.
        });

        // Player container.
        this.attachElement(this, "playerContainer", ".webamp-player-container", {
            eventName: "click",
            callback: this.expand   // Always expand when the player itself is clicked.
        },{
            eventName: "mouseleave",
            callback: this.shrink   // Always shrink when the mouse leaves the player.
        });

        // Playlist button.
        this.attachElement(this, "playlistButton", ".webamp-button-playlist", {
            eventName: "click",
            callback: () => this.showPanel("playlist")  // Switch panels on click.
        });

        // Playlist container.
        this.attachElement(this, "playlistContainer", ".webamp-panel.webamp-playlist");

        // EQ button.
        this.attachElement(this, "eqButton", ".webamp-button-eq", {
            eventName: "click",
            callback: () => this.showPanel("eq")    // Switch panels on click.
        });

        // EQ container.
        this.attachElement(this, "eqContainer", ".webamp-panel.webamp-eq");

        // Playlist button container.
        this.attachElement(this, "playlistButtonContainer", ".webamp-button-playlist-container");

        // EQ button container.
        this.attachElement(this, "eqButtonContainer", ".webamp-button-eq-container");

        // EQ sliders.
        this.attachMultipleElements(this, "eqSliders", "[audio-button-eq-range]")
        .then((elements) => {
            elements.map((element) => {
                // Listen for changes in the range sliders so that we can automatically refresh
                // the style for each element.
                element.addEventListener("input", () => this.setBackgroundGradient(element));
            });
        });

        // Playlist visualiser.
        this.attachElement(this, "playlistVisualiserContainer", ".webamp-playlist .html-visualiser-container");

        // Playlist playtime.
        this.attachElement(this, "playlistPlaytimeContainer", ".webamp-playlist .playtime-container");
    }

    /**
     * Called when the current track is changed. Allows the theme to move the playlist and
     * playtime components to the correct vertical locations.
     * @private
     * @param {Number} index The index of the track that was selected.
     */
    onTrackChanged = (index) => {
        this.playlistVisualiserContainer.style.top = `${index * this._playlistItemHeight + 15}px`;
        this.playlistPlaytimeContainer.style.top = `${index * this._playlistItemHeight + 16}px`;
    }

    /**
     * Build the configuration that will provide the components and layouts for
     * this theme.
     * @public
     * @returns {WebAmpConfigurer} The configurer that was built.
     */
    buildConfiguration() {

        this.startConfiguring()
        // We're playing audio, so we need a pipeline.
        .addAudioPipeline()
            // We want to stream songs, so add a player with some defaults.
            .withAudioPlayer().withSettings({
                autoPlay: true, 
                pauseOnHide: true
            })
            // We want a playlist.
            .and().withPlaylistManager()
            // We want the user to be able to set EQ settings.
            .and().withEqualizer().withSettings({
                filterPreferences: {
                    minFrequencyGain: -12,
                    maxFrequencyGain: 12
                },
                presets: [
                    { name: "Bass", bands: new Map([[400,6],[600,12],[800,8],[1000,0],[2000,-8],[3000,-12],[4000,-6]]) },
                    { name: "Rock", bands: new Map([[400,8],[600,4],[800,0],[1000,-5],[2000,0],[3000,4],[4000,8]]) },
                    { name: "Club", bands: new Map([[400,0],[600,4.5],[800,8],[1000,9],[2000,8],[3000,4.5],[4000,0]]) }
                ]
            })
            // We want the visualiser in the playlist to be a distinct set of elements.
            // We'll override the name so that we can have more than one of these components
            // in the theme.
            .and().withAudioHtmlVisualiser().withName("PlaylistVisualiser").withSettings({
                mutationHtmlAttributeName: "audio-vis-eq-frequency-playlist",    // We'll need to use a different attribute in the markup.
                animationFrameLength: 17    // Target 60fps.
            })
            // We want the developer to be able to manipulate HTML elements dynamically.
            .and().withAudioHtmlVisualiser()
            .and().then()
            // We only have a single layout for this configuration.
        .addLayout(layout).withStyles(styles)
        // Done!
        .finish();
    }

    /**
     * Expand the player so that the full controls are visible.
     * @method
     * @public
     */
    expand = () => {
        this.playerContainer.classList.add("expanded");

        if(this.selectedPanel == "playlist") {
            this.playlistContainer.style.setProperty("height", `${this.playlistHeight}px`);
        }

        this.expanded = true;
    }

    /**
     * Shrink the player so that only the compact controls are visible.
     * @method
     * @public
     */
    shrink = () => {
        this.playerContainer.classList.remove("expanded");
        this.playlistContainer.style.removeProperty("height");

        this.expanded = false;
    }

    /**
     * Show a specific panel to the user to manipulate audio.
     * @method
     * @public
     * @param {String} panelName The name of the panel to show ("playlist" or "eq").
     */
    showPanel = (panelName) => {

        if(!this.eqButtonContainer || !this.playlistButtonContainer) {
            return;
        }
        
        // Make sure the "on" class is on the right button container.
        switch(panelName) {
            case "eq":
                this.playlistButtonContainer.classList.remove("on");
                this.eqButtonContainer.classList.add("on");
                this.eqContainer.classList.add("on");

                this.playlistContainer.style.removeProperty("height");
                this.playlistContainer.classList.remove("on");
                break;
            case "playlist":
            default:
                this.eqButtonContainer.classList.remove("on");
                this.playlistButtonContainer.classList.add("on");
                this.eqContainer.classList.remove("on");
                this.playlistContainer.classList.add("on");
                if(this.expanded) {
                    // We need to set the height of the playlist because we're expanded and that's
                    // the panel that should be visible.
                    this.playlistContainer.style.setProperty("height", `${this.playlistHeight}px`);
                }
                break;
        }

        // Save the selected panel.
        this.selectedPanel = panelName;
        this.storeValue("selectedPanel", panelName);
    }

    /**
     * Recalculate the total height that the playlist should be, based on the number of items
     * that it should contain.
     * @private
     * @param {Number} trackCount The number of tracks to account for.
     */
    recalculatePlaylistHeight = (trackCount) => {
        this.playlistHeight = trackCount * this._playlistItemHeight + this._playlistBottomPadding;

        if(this.expanded && this.selectedPanel == "playlist") {
            // The players is expanded and the visible panel is the playlist, so set its height.
            this.playlistContainer.style.setProperty("height", `${this.playlistHeight}px`);
        }
    }

    /**
     * Set the background gradient on an HTML range element. This is necessary in order to
     * have the filled part of the slider show a different color than the empty part.
     * @private
     * @param {HTMLElement} element The element to set background gradient on.
     */
    setBackgroundGradient = (element) => {
        // Normalise the value to a percentage (from +/- 12).
        const sliderPercentage = ((parseFloat(element.value) + this._frequencyMaxDistortion) / (2 * this._frequencyMaxDistortion)) * 100;
        // Now set the value.
        element.style.background = "linear-gradient(to right, #69d7e2 0%, #69d7e2 " + sliderPercentage + "%, #616274 " + sliderPercentage + "%, #616274 100%)";
    }
 
    /**
     * Called whenever a new frequency is selected for a particular band.
     * @private
     * @param  {...any} args The arguments provided by the frequencyChanged event.
     */
    onFrequencyChanged = (...args) => {
        // Args is:
        // [0]: frequency band
        // [1]: frequency level
        // [2]: range input
        this.setBackgroundGradient(args[2]);
    }
};

module.exports = { MinimalTheme };

new MinimalTheme().register();