## [1.0.5](https://bitbucket.org/zyrous/z-amp-minimal/compare/v1.0.4...v1.0.5) (2021-03-31)


### Bug Fixes

* updated to z-amp-core v1.2.2 ([8d96356](https://bitbucket.org/zyrous/z-amp-minimal/commits/8d96356a98144ecd56651763e25fc1785ce3c75a))

## [1.0.4](https://bitbucket.org/zyrous/z-amp-minimal/compare/v1.0.3...v1.0.4) (2021-03-23)


### Bug Fixes

* adding vertical alignment for codepen issue ([90242ea](https://bitbucket.org/zyrous/z-amp-minimal/commits/90242ea6ffdbe3f449ba2ed44752e2cd4c66e80c))

## [1.0.3](https://bitbucket.org/zyrous/z-amp-minimal/compare/v1.0.2...v1.0.3) (2021-03-23)


### Bug Fixes

* correcting tests ([350c1e2](https://bitbucket.org/zyrous/z-amp-minimal/commits/350c1e2e5c886343a3c64d4cb6d3e007b4b0641b))
* increasing unit test timeout ([e389725](https://bitbucket.org/zyrous/z-amp-minimal/commits/e38972557b5a919ce9e301dd363fa493f41ab13d))
* updating module exports to fix circular dependancy ([f04612f](https://bitbucket.org/zyrous/z-amp-minimal/commits/f04612fb34402f2b07a078e87fc8fc87651e5d2d))
* upgrading z-amp-core version ([ce41707](https://bitbucket.org/zyrous/z-amp-minimal/commits/ce4170755e6b751ae3bf98eaece0824be6ce8626))

## [1.0.2](https://bitbucket.org/zyrous/z-amp-minimal/compare/v1.0.1...v1.0.2) (2021-03-09)


### Bug Fixes

* corrected URL of bundle in README sample ([56057d9](https://bitbucket.org/zyrous/z-amp-minimal/commits/56057d975d46804ab858fd86b8f50e74b32e0b86))

## [1.0.1](https://bitbucket.org/zyrous/z-amp-minimal/compare/v1.0.0...v1.0.1) (2021-03-09)


### Bug Fixes

* adding documentation and image ([38fbc77](https://bitbucket.org/zyrous/z-amp-minimal/commits/38fbc7715576ec118f510e677c4d1a15f14e7633))

# 1.0.0 (2021-03-09)


### Features

* adding initial files ([434b9d3](https://bitbucket.org/zyrous/z-amp-minimal/commits/434b9d317dfe8424a56e161ba341255587856fc1))
